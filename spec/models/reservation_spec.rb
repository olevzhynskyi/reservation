require 'spec_helper'

describe Reservation do

  context "validations" do

    context "default" do
      it { should validate_presence_of(:start_time) }
      it { should validate_presence_of(:end_time) }
      it { should validate_presence_of(:table) }
      it { should validate_numericality_of(:table).only_integer }
    end

    context "for overbooking" do
      before(:each) do
        create(:reservation, start_time: "10.10.2014 12:00", end_time: "10.10.2014 13:00", table: 1)
      end

      it "should create reservation for new time without errors" do
        reservation = build(:reservation, start_time: "10.10.2014 20:00", end_time: "10.10.2014 20:30", table: 1)
        expect(reservation.errors).to be_empty
      end

      it "should fail validation if start time inside existing interval" do
        reservation = build(:reservation, start_time: "10.10.2014 12:30", end_time: "10.10.2014 14:00", table: 1)
        expect(reservation).to have(1).error_on(:base)
      end

      it "should fail validation if end time inside existing interval" do
        reservation = build(:reservation, start_time: "10.10.2014 10:00", end_time: "10.10.2014 12:30", table: 1)
        expect(reservation).to have(1).error_on(:base)
      end

      it "should fail validation if new interval inside existing interval" do
        reservation = build(:reservation, start_time: "10.10.2014 12:10", end_time: "10.10.2014 12:20", table: 1)
        expect(reservation).to have(1).error_on(:base)
      end

      it "should fail validation if new interval wrap existing interval" do
        reservation = build(:reservation, start_time: "10.10.2014 10:00", end_time: "10.10.2014 14:00", table: 1)
        expect(reservation).to have(1).error_on(:base)
      end
    end

    context "for time interval" do
      it "should fails if start time less then end time" do
        reservation = Reservation.new(start_time: "10.10.2014 2:00", end_time: "10.10.2014 1:00", table: 1)
        expect(reservation).to have(1).error_on(:start_time)
      end
    end
  end

end
