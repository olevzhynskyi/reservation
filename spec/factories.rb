FactoryGirl.define do
  factory :reservation do
    start_time  { Time.now }
    end_time    { Time.now + 1.hour }
    table       { 1 + rand(100) }
  end
end