class Reservation < ActiveRecord::Base
  validates :end_time, presence: true
  validates :table, presence: true, numericality: { only_integer: true }
  validates :start_time, presence: true

  validate :table_overbooking
  validate :start_time_checker

  scope :by_time_interval, ->(start_time, end_time) {
    where("start_time BETWEEN :start AND :end
        OR end_time BETWEEN :start AND :end
        OR :start BETWEEN start_time AND end_time",
        { start: start_time,  end: end_time })
  }

  private

  def start_time_checker
    errors.add(:start_time, "must be less then end time") if start_time.nil? || end_time.nil? || start_time > end_time
  end

  def table_overbooking
    if Reservation.by_time_interval(start_time, end_time).where(table: table).size != 0
      errors[:base] << "Booking for that time already exist"
    end
  end

end
